import puppeteer, { Browser, BrowserContext, ElementHandle, Page } from "puppeteer"
import { roundAtDecimal } from "./utils/roundAtDecimal"
import { getRandomIntBetween } from "./utils/getRandomInt"
import { LandingPage } from "./page-objects/LandingPage"
import { ProductPage } from "./page-objects/ProductPage"

const url = "https://www.cdiscount.com"

const acceptCookies = async (landingPage : LandingPage)=> {
  const button : ElementHandle<HTMLButtonElement> = await landingPage.acceptCookiesBtn()
  await button.click({ delay: 1107 })
}

const searchItems = async (landingPage : LandingPage, searchValue : string)=> {
  const searhBar : ElementHandle<HTMLInputElement> = await landingPage.searchBar()
  await searhBar.type(searchValue, { delay: roundAtDecimal(getRandomIntBetween(201, 1100), 0) })
  await searhBar.press("Enter", { delay: 356 })
}

const gotoFirstProduct = async (productPage : ProductPage)=> {
  const anchor : ElementHandle<HTMLAnchorElement> = (await productPage.firstProductTitleAnchor())
  await anchor.click({ delay: 956 })
}

const app = async () => {

  //Init browser
  const browser : Browser = await puppeteer.launch({
    userDataDir: ("./user_data"),
    headless: true
  })
  const context : BrowserContext = await browser.createIncognitoBrowserContext()
  const page : Page = await context.newPage()
  await page.setViewport({ width: 1200, height: 800 })

  await page.goto(url, { waitUntil: "networkidle2" })
  const landingPage = new LandingPage(page)
  const productPage = new ProductPage(page)

  //Scrapping
  await acceptCookies(landingPage)
  await searchItems(landingPage, "ordinateur")
  await page.waitForNavigation({ timeout: 10000, waitUntil: "networkidle2" })
  await gotoFirstProduct(productPage)

  //Close browser
  await browser.close()
}

app().catch(err=> console.log("An error occured", err))
