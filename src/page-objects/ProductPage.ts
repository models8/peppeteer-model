import { ElementHandle, Page } from "puppeteer"

export class ProductPage {

  constructor (private page: Page) {}

  async firstProductTitleAnchor () : Promise<ElementHandle<HTMLAnchorElement>> {
    const titleAnchor : ElementHandle<HTMLAnchorElement>|null = await this.page.waitForSelector(
      "a.jsPrdtBILA, a.prdtBILA" as any,
      { timeout: 10000 }
    )

    if (!titleAnchor)
      throw "No search bar"

    return titleAnchor
  }

}
