import { ElementHandle, Page } from "puppeteer"

export class LandingPage {

  constructor (private page: Page) {}

  async searchBar () : Promise<ElementHandle<HTMLInputElement>> {
    const searhBar : ElementHandle<HTMLInputElement>|null = await this.page.waitForSelector(
      "div.hSrcInput > input[type=search]" as any,
      { timeout: 10000 }
    )

    if (!searhBar)
      throw "No search bar"

    return searhBar
  }

  async acceptCookiesBtn () : Promise<ElementHandle<HTMLButtonElement>> {
    const cookiebtn : ElementHandle<HTMLButtonElement>|null = await this.page.waitForSelector(
      "button#footer_tc_privacy_button_2" as any,
      { timeout: 3000 }
    )
    if (!cookiebtn)
      throw "No search bar"

    return cookiebtn

  }

}
