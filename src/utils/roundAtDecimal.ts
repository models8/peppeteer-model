export const roundAtDecimal = (floatNumber: number, decimals: number) => {
  const truncatNumber : string = floatNumber.toFixed(decimals)
  return parseFloat(truncatNumber)
}
