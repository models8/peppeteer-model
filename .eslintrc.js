module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    sourceType: 'module',
    project: [`tsconfig.build.json`,'tsconfig.json']
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  env: {
    node: true,
    jest: true
  },
  ignorePatterns: ['.eslintrc.js', 'swagger*'],
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    /*--------My own rules ----------------*/
    '@typescript-eslint/no-inferrable-types': 0,
    'import/no-unresolved': 0,
    'import/no-cycle': 1,
    'import/prefer-default-export': 0,
    '@typescript-eslint/semi': [2, 'never'],
    "indent": ["error", 2, { "SwitchCase": 1 }],
    'max-len': [2, 100, { ignoreComments: true, ignoreStrings: true }],
    'no-unexpected-multiline': 2,
    'no-underscore-dangle': 0,
    'no-continue': 0,
    'brace-style': ["error", "stroustrup"],
    'quotes': ["error", "double"],
    "comma-dangle": ["error", "never"],
    "space-in-parens": ["error", "never"],
    "comma-spacing": ["error", { "before": false, "after": true }],
    "curly": [2, "multi-or-nest", "consistent"],
    "nonblock-statement-body-position": [2, "below"],
    "space-infix-ops": "error",
    "object-curly-spacing": ["error", "always"],
    "keyword-spacing": ["error", { "before": true, "after": true }],
    "key-spacing": ["error", { "beforeColon": false, "afterColon": true }],
    "array-bracket-spacing": ["error", "never"],
    "space-before-function-paren": ["error", {
      "anonymous": "always",
      "named": "always",
      "asyncArrow": "always"
    }],
    "no-trailing-spaces": ["error", { "skipBlankLines": true }],
    "no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 0, "maxBOF": 0 }],
    "no-multi-spaces": "error"

  }
}
